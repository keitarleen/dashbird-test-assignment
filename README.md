# Dashbird front-end test assignment

## To run project locally

Navigate into project folder and run `yarn yarn start`

Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

## Project is also hosted on [https://dashbird-test.web.app/](https://dashbird-test.web.app/)
