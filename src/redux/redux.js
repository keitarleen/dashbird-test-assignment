import { createStore } from "redux";

const initialState = {
  selectedData: "32",
  lineData: [
    { x: 0, y: 3 },
    { x: 0.1, y: 3 },
    { x: 0.2, y: 4 },
    { x: 0.3, y: 6 },
    { x: 0.4, y: 7 },
    { x: 0.5, y: 8 },
    { x: 0.6, y: 9 },
    { x: 0.7, y: 9.1 },
    { x: 0.8, y: 3 },
    { x: 0.9, y: 3 },
    { x: 1, y: 5 },
    { x: 1.2, y: 6 },
    { x: 1.3, y: 11 },
    { x: 1.5, y: 1 },
    { x: 1.6, y: 2 },
    { x: 1.8, y: 4 },
    { x: 1.9, y: 8 },
    { x: 2, y: 2 },
    { x: 2.1, y: 3 },
    { x: 2.2, y: 3.5 },
    { x: 2.5, y: 4 },
    { x: 2.6, y: 5 },
    { x: 2.7, y: 5 },
    { x: 2.9, y: 5.9 },
    { x: 3.4, y: 2.2 },
    { x: 3.5, y: 2.2 },
    { x: 3.6, y: 2.2 },
    { x: 3.7, y: 2.2 },
    { x: 3.8, y: 2.2 },
    { x: 3.9, y: 2.2 },
    { x: 4, y: 8.8 },
    { x: 5, y: 7 },
    { x: 6, y: 6 },
    { x: 7, y: 2 },
    { x: 8, y: 3 },
    { x: 9, y: 8 },
    { x: 10, y: 0 },
  ],
  positiveData: [
    { x: 0, y: 8 },
    { x: 0.1, y: 3 },
    { x: 1, y: 5 },
    { x: 2, y: 4 },
    { x: 3, y: 9 },
    { x: 4, y: 1 },
    { x: 5, y: 7 },
    { x: 6, y: 6 },
    { x: 7, y: 3 },
    { x: 8, y: 2 },
    { x: 9, y: 0 },
  ],
  negativeData: [
    { x: 0, y: -8 },
    { x: 0.4, y: -3 },
    { x: 1.5, y: -5 },
    { x: 2.5, y: -4 },
    { x: 3.5, y: -9 },
    { x: 4.5, y: -1 },
    { x: 5.5, y: -7 },
    { x: 6.5, y: -6 },
    { x: 7.5, y: -3 },
    { x: 8.5, y: -2 },
    { x: 9.5, y: -3 },
  ],
};

export const store = createStore(reducer, initialState);

function reducer(state = initialState, action) {
  switch (action.type) {
    case "SET_SELECTED_DATA":
      return {
        ...state,
        selectedData: action.payload,
      };
    default:
      return state;
  }
}

export default reducer;
