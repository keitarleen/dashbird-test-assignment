export const theme = {
  color: {
    primary: "#4a45c6",
    secondary: "#c7c4c4",
    white: "#ffffff",
    red: "#DC143C",
    greyDark: "#f0f0f0",
  },
};
