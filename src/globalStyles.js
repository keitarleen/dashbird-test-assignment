import { createGlobalStyle } from "styled-components";

const GlobalStyle = createGlobalStyle`
    body {
    margin: 0;
    font-family: "Roboto", sans-serif;
    background-color: #f5f6fa;
    font-size: 16px;
    }

    button {
    font-family: inherit;
    }
`;

export default GlobalStyle;
