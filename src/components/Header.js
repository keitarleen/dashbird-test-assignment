import React from "react";
import styled from "styled-components";
import userIcon from "../assets/user-icon.svg";
import Arrow from "./Arrow";

const Header = () => {
  return (
    <HeaderContainer>
      <Menu>
        Dashsbird.io
        <MenuArrow>
          <Arrow direction='down' />
        </MenuArrow>
      </Menu>
      <AccountButton>
        <UserIcon src={userIcon} />
      </AccountButton>
    </HeaderContainer>
  );
};

export default Header;

const HeaderContainer = styled.nav`
  background-color: ${({ theme }) => theme.color.primary};
  height: 60px;
  display: flex;
  align-items: center;
  justify-content: flex-end;
  padding: 0 20px;
`;

const AccountButton = styled.button`
  width: 30px;
  height: 30px;
  border: none;
  background-color: ${({ theme }) => theme.color.secondary};
  border-radius: 100%;
  display: flex;
  justify-content: center;
  align-items: center;
  cursor: pointer;
`;

const UserIcon = styled.img`
  height: 30px;
  width: 30px;
`;

const Menu = styled.button`
  border: none;
  text-transform: uppercase;
  font-size: 16px;
  color: ${({ theme }) => theme.color.secondary};
  background-color: inherit;
  padding: 0 10px;
  display: flex;
  align-items: center;
  cursor: pointer;
  margin-right: 20px;
`;

const MenuArrow = styled.div`
  margin-left: 10px;
`;
