import React from "react";
import styled from "styled-components";
import settingsIcon from "../assets/settings-icon.svg";
import linkIcon from "../assets/link-icon.svg";
import ChartTab from "./ChartTab";
import FooterColumn from "./FooterColumn";
import Chart from "./Chart";
import { useSelector } from "react-redux";

const Landing = () => {
  const selectedValue = useSelector((state) => state.selectedData);
  const lineData = useSelector((state) => state.lineData);

  return (
    <>
      <LandingNav>
        <NavTitle>DASHBIRD.IO - some string - some string</NavTitle>
        <NavLinks>
          <NavLink href=''>
            AWS console
            <NavLinkIcon src={linkIcon} />
          </NavLink>
          <NavLink href=''>
            Configuration
            <NavLinkIcon src={settingsIcon} />
          </NavLink>
        </NavLinks>
      </LandingNav>
      <Title>
        <TitleIcon /> alerting-check-policy-conditions
      </Title>
      <ChartNav>
        <ChartTab
          title='Avg. response delay'
          unit='23ms'
          chartData={lineData}
        />
        <ChartTab
          title='Last que size'
          unit=''
          selectedDatapoint={selectedValue}
          isTabActive={true}
        />
        <ChartTab
          title='Avg. payload size'
          unit='1.35kb'
          chartData={lineData}
        />
        <ChartTab title='Dead letter que' unit='0' chartData={lineData} />
      </ChartNav>
      <ChartContainer>
        <Chart />
      </ChartContainer>
      <Footer>
        <FooterColumn title='Resources' />
        <FooterColumn title='Insights' />
        <FooterColumn title='Alerts' />
      </Footer>
    </>
  );
};

export default Landing;

const TitleIcon = styled.div`
  display: inline-block;
  width: 40px;
  height: 40px;
  border-color: ${({ theme }) => theme.color.white};
  border-radius: 5px;
  background-color: ${({ theme }) => theme.color.white};
  margin-right: 10px;
`;

const Title = styled.h1`
  margin: 16px;
  font-weight: 300;
  display: flex;
  align-items: center;

  @media (min-width: 768px) {
    margin: 0;
  }
`;

const LandingNav = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  margin-top: 24px;
  height: 24px;
`;

const NavTitle = styled.p`
  margin: 0;
  font-size: 12px;
  color: ${({ theme }) => theme.color.secondary};
  text-transform: uppercase;
`;

const NavLinks = styled.div`
  display: flex;
  flex-direction: column;
  align-items: flex-end;

  @media (min-width: 768px) {
    flex-direction: row;
    align-items: center;
  }
`;

const NavLink = styled.a`
  text-decoration: none;
  color: ${({ theme }) => theme.color.primary};
  text-transform: uppercase;
  font-size: 12px;
  display: inline-flex;
  align-items: center;

  :first-of-type {
    @media (min-width: 768px) {
      margin-right: 20px;
    }
  }
`;

const NavLinkIcon = styled.img`
  height: 16px;
  width: 16px;
  margin-left: 5px;
`;

const ChartNav = styled.section`
  display: flex;
  margin-top: 16px;
`;

const ChartContainer = styled.section`
  background-color: ${({ theme }) => theme.color.white};
  height: 320px;
  width: 100%;
`;

const Footer = styled.section`
  margin-top: 8px;
  display: flex;
  flex-direction: column;
  margin-left: -10px;
  margin-right: -10px;

  @media (min-width: 768px) {
    flex-direction: row;
  }
`;
