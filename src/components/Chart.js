import React, { useState } from "react";
import { theme } from "../theme";
import "../../node_modules/react-vis/dist/style.css";
import { useDispatch, useSelector } from "react-redux";

import {
  LineSeries,
  VerticalBarSeries,
  XAxis,
  YAxis,
  Crosshair,
  DiscreteColorLegend,
  FlexibleWidthXYPlot,
} from "react-vis";

const Chart = () => {
  const [points, setPoints] = useState([]);
  const [showCrosshair, setShowCrosshair] = useState(false);
  const lineData = useSelector((state) => state.lineData);
  const negativeData = useSelector((state) => state.negativeData);
  const positiveData = useSelector((state) => state.positiveData);

  const dispatch = useDispatch();

  const xAxis = [
    { x: 0, y: 0 },
    { x: 10, y: 0 },
  ];

  return (
    <>
      <FlexibleWidthXYPlot
        height={280}
        onMouseLeave={() => setPoints([])}
        onClick={(e) => setShowCrosshair(true)}
      >
        <LineSeries
          data={lineData}
          curve='curveBasis'
          stroke={theme.color.secondary}
          style={{ strokeWidth: 1 }}
          onNearestX={(datapoint, event) => {
            if (showCrosshair) {
              dispatch({ type: "SET_SELECTED_DATA", payload: datapoint.y });
              setPoints([datapoint]);
            }
          }}
        />
        <VerticalBarSeries
          data={positiveData}
          barWidth={0.3}
          color={theme.color.red}
        />
        <VerticalBarSeries
          data={negativeData}
          barWidth={0.3}
          color={theme.color.primary}
        />
        {showCrosshair && (
          <Crosshair values={points}>
            <div></div>
          </Crosshair>
        )}
        <LineSeries
          data={xAxis}
          stroke={theme.color.secondary}
          style={{ strokeWidth: 1 }}
        />
        <DiscreteColorLegend
          colors={[theme.color.primary, theme.color.red, theme.color.secondary]}
          items={["pending increased", "pending resolved", "last que size"]}
          orientation='horizontal'
        />
        <XAxis />
        <YAxis />
      </FlexibleWidthXYPlot>
    </>
  );
};

export default Chart;
