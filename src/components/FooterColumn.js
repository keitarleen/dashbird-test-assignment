import React from "react";
import styled from "styled-components";
import clockIcon from "../assets/clock-icon.svg";

const FooterColumn = ({ title }) => {
  return (
    <ColContainer>
      <ColHeader>
        <ColTitle>{title}</ColTitle>
        <FilterDataButton>Execution time</FilterDataButton>
      </ColHeader>
      <ColBlock>
        <BlockIcon src={clockIcon} />
        <BlockInfo>
          <Info>usage-service-prod-record-inventory-usage</Info>
          <ExtraInfo>125ms</ExtraInfo>
        </BlockInfo>
      </ColBlock>
      <ColBlock>
        <BlockIcon src={clockIcon} />
        <BlockInfo>
          <Info>susage-service-prod-record-inventory-usage</Info>
          <ExtraInfo>125ms</ExtraInfo>
        </BlockInfo>
      </ColBlock>
      <ColBlock>
        <BlockIcon src={clockIcon} />
        <BlockInfo>
          <Info>usage-service-prod-record-inventory-usage</Info>
          <ExtraInfo>125ms</ExtraInfo>
        </BlockInfo>
      </ColBlock>
    </ColContainer>
  );
};

export default FooterColumn;

const ColContainer = styled.div`
  width: 100%;
  margin-right: 5px;
  margin-left: 5px;

  @media (min-width: 768px) {
    width: 33.3%;
  }
`;

const ColHeader = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
  height: 24px;
`;

const FilterDataButton = styled.button`
  border: none;
  background-color: transparent;
  font-size: 12px;
  display: flex;
  color: ${({ theme }) => theme.color.secondary}; ;
`;

const ColTitle = styled.h5`
  font-size: 12px;
  font-weight: 300;
  text-transform: uppercase;
  color: ${({ theme }) => theme.color.secondary};
  margin: 0;
`;

const ColBlock = styled.div`
  height: 48px;
  background-color: ${({ theme }) => theme.color.white};
  padding: 10px;
  display: flex;

  :not(:first-of-type) {
    margin-top: 2px;
  }
`;

const BlockIcon = styled.img`
  width: 20px;
  height: 20px;
  margin-right: 10px;
`;

const BlockInfo = styled.div`
  display: flex;
  flex-direction: column;
  border-left: 1px solid ${({ theme }) => theme.color.secondary};
  padding-left: 10px;
`;

const Info = styled.p`
  margin: 0;
  font-size: 14px;
  line-height: 18px;
`;

const ExtraInfo = styled.p`
  margin: 0;
  font-size: 14px;
  line-height: 18px;
  color: ${({ theme }) => theme.color.secondary};
`;
