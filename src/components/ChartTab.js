import React from "react";
import styled from "styled-components";
import clockIcon from "../assets/clock-icon.svg";
import "../../node_modules/react-vis/dist/style.css";
import { FlexibleWidthXYPlot, LineSeries } from "react-vis";
import { theme } from "../theme";

const ChartTab = ({
  selectedDatapoint,
  isTabActive,
  title,
  unit,
  chartData,
}) => {
  return (
    <TabContainer active={isTabActive}>
      <TabNav>
        <TabTitle>{title}</TabTitle>
        <TabIcon src={clockIcon} />
      </TabNav>
      <TabData active={isTabActive}>
        {selectedDatapoint} {unit}
      </TabData>
      <TabChart>
        {!isTabActive && (
          <FlexibleWidthXYPlot height={24}>
            <LineSeries
              data={chartData}
              stroke={theme.color.secondary}
              style={{
                transform: "translate(0px, 25px)",
                strokeWidth: 1,
              }}
            />
          </FlexibleWidthXYPlot>
        )}
      </TabChart>
    </TabContainer>
  );
};

export default ChartTab;

const TabContainer = styled.button`
  border: none;
  cursor: pointer;
  font-weight: 300;
  text-align: left;
  display: flex;
  flex-direction: column;
  border-left: 1.5px solid
    ${({ active, theme }) =>
      active ? theme.color.primary : theme.color.secondary};
  padding: 5px 10px;
  height: 72px;
  width: 200px;
  background-color: ${({ active, theme }) =>
    active ? theme.color.white : "transparent"};

  :hover {
    background-color: ${({ active, theme }) =>
      !active ? theme.color.greyDark : theme.color.white};
  }

  :focus {
    outline: none;
  }
`;

const TabNav = styled.div`
  display: flex;
  justify-content: space-between;
  width: 100%;
`;

const TabTitle = styled.h5`
  font-size: 12px;
  font-weight: 300;
  text-transform: uppercase;
  color: ${({ theme }) => theme.color.secondary};
  margin: 0;
`;

const TabIcon = styled.img`
  width: 15px;
  height: 15px;
`;

const TabData = styled.p`
  font-size: ${({ active }) => (active ? "24" : "18")}px;
  font-weight: 300;
  margin: 0;

  @media (min-width: 768px) {
    font-size: ${({ active }) => (active ? "40" : "24")}px;
  }
`;

const TabChart = styled.div`
  display: none;

  @media (min-width: 768px) {
    display: block;
    width: 100%;
  }
`;
