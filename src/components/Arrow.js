import React from "react";
import styled from "styled-components";

const Arrow = ({ direction }) => (
  <ArrowContainer direction={direction}>
    <ArrowLineLeft />
    <ArrowLine />
  </ArrowContainer>
);

export default Arrow;

const ArrowContainer = styled.div`
  transform: ${(direction) => (direction === "up" ? "rotate(90deg)" : "none")};
  height: 100%;
  width: 20px;
`;

const ArrowLine = styled.span`
  display: inline-block;
  width: 3px;
  height: 10px;
  background-color: ${({ theme }) => theme.color.secondary};
  transform: rotate(45deg);
  margin-left: 3px;
`;

const ArrowLineLeft = styled.span`
  display: inline-block;
  width: 3px;
  height: 10px;
  background-color: ${({ theme }) => theme.color.secondary};
  transform: rotate(-45deg);
`;
