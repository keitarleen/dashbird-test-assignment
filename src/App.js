import React from "react";
import styled, { ThemeProvider } from "styled-components";
import Header from "./components/Header";
import Landing from "./components/Landing";
import { theme } from "./theme";
import GlobalStyle from "./globalStyles";

function App() {
  return (
    <ThemeProvider theme={theme}>
      <GlobalStyle />
      <Header />
      <Layout>
        <Landing />
      </Layout>
    </ThemeProvider>
  );
}

const Layout = styled.div`
  padding: 0 20px;
  height: 100%;
`;

export default App;
